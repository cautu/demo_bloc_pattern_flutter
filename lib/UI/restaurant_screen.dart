import 'package:flutter/material.dart';
import 'package:restaurant_finder/BLoC/bloc_provider.dart';
import 'package:restaurant_finder/BLoC/restaurant_bloc.dart';
import 'package:restaurant_finder/UI/favorite_screen.dart';
import 'package:restaurant_finder/UI/location_screen.dart';

import '../DataLayer/location.dart';
import '../DataLayer/restaurant.dart';
import "./restaurant_title.dart";

class RestaurantScreen extends StatelessWidget {
  final Location location;

  const RestaurantScreen({Key key, this.location}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(location.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.favorite_border),
            onPressed: () => Navigator.of(context)
                .push(MaterialPageRoute(builder: (_) => FavoriteScreen())),
          )
        ],
      ),
      body: _buildSearch(context),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.edit_location),
        onPressed: () => Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => LocationScreen(
                  isFullScreenDialog: true,
                ),
            fullscreenDialog: true)),
      ),
    );
  }

  Widget _buildSearch(BuildContext context) {
    final bloc = RestaurantBloc(location);
    return BlocProvider<RestaurantBloc>(
      bloc: bloc,
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10.0),
            child: TextField(
              decoration: InputDecoration(
                hintText: "What do you want to eat?",
                border: OutlineInputBorder(),
              ),
              onChanged: (query) {
                bloc.submitQuery(query);
              },
            ),
          ),
          Expanded(
            child: _buildStreamBuilder(bloc),
          ),
        ],
      ),
    );
  }

  Widget _buildStreamBuilder(RestaurantBloc bloc) {
    return StreamBuilder(
      stream: bloc.stream,
      builder: (context, snapshot) {
        final result = snapshot.data;
        if (result == null) {
          return Center(
            child: Text("Enter a restaurant name or cuisine"),
          );
        }
        if (result.isEmpty) {
          return Center(
            child: Text("No results"),
          );
        }
        return _buildSearchResult(result);
      },
    );
  }

  Widget _buildSearchResult(List<Restaurant> result) {
    return ListView.separated(
      itemCount: result.length,
      separatorBuilder: (context, index) => Divider(),
      itemBuilder: (context, index) {
        final restaurant = result[index];
        return RestaurantTile(restaurant: restaurant);
      },
    );
  }
}
