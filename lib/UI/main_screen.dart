import 'package:flutter/material.dart';
import 'package:restaurant_finder/BLoC/bloc_provider.dart';
import 'package:restaurant_finder/BLoC/location_bloc.dart';
import '../DataLayer/location.dart';
import "./location_screen.dart";
import "./restaurant_screen.dart";

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return StreamBuilder<Location>(
      stream: BlocProvider.of<LocationBloc>(context).locationStream,
      builder: (context, snapshot) {
        final location = snapshot.data;
        // print('main-screen, $location');
        if (location == null) {
          return LocationScreen();
        }
        return RestaurantScreen(
          location: location,
        );
      },
    );
  }
}
